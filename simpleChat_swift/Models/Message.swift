//
//  Message.swift
//  simpleChat_swift
//
//  Created by Anton Medvediev on 31/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
