//
//  ViewController.swift
//  simpleChat_swift
//
//  Created by Anton Medvediev on 27/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = ""
        var i = 0.0
        let titleText = K.appName
        for letter in titleText{
            Timer.scheduledTimer(withTimeInterval: 0.1*i, repeats: false){(timer) in self.titleLabel.text?.append(letter)
            }
            i+=1
        }
    }
    
    
}

